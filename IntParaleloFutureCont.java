/**
 * Práctica 6 - PCTR
 * 2017/18
 *
 * @author Iván Magariño Aguilar
 */

import java.util.Scanner;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.Random;
import java.util.concurrent.Future;
import java.util.concurrent.*;
import java.util.ArrayList;

public class IntParaleloFutureCont implements Callable {
    
    public Integer cont;
    
    public int lInf, lSup;
    
    public Random r;

    /**
     * Constructor de la clase
     * @param lInf limite inferior del subrango
     * @param lSup limite superior del subrango
     */
    public IntParaleloFutureCont(int lInf, int lSup) {
        this.lInf = lInf;
        this.lSup = lSup;
        cont = new Integer(0);
        r = new Random(System.currentTimeMillis());
    }
    
    /**
     * Sobrecarga del metodo call
     */
    public Integer call() {
        for (int i = lInf; i < lSup; i++) {
            if (r.nextDouble() < Math.sin(r.nextDouble())) {
                cont++;
            }
        }
        return cont;
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {

        int nProcesos = Runtime.getRuntime().availableProcessors();
        Scanner s = new Scanner(System.in);
        System.out.print("Introduzca un numero de puntos: ");
        int nPuntos = s.nextInt();
        int tVentana = nPuntos / nProcesos;
        int lInf = 0;
        int lSup = tVentana;

        long iniCronom = System.currentTimeMillis();
        ThreadPoolExecutor pool = new ThreadPoolExecutor(nProcesos, nProcesos, 1000L, TimeUnit.MINUTES,
                new LinkedBlockingQueue<Runnable>());

        ArrayList<Future<Integer>> future = new ArrayList<Future<Integer>>();
        for (int i = 0; i < nProcesos; i++) {
            if (i == nProcesos - 1) {
                future.add(pool.submit(new IntParaleloFutureCont(lInf, Math.max(lSup, nPuntos))));
            } else {
                future.add(pool.submit(new IntParaleloFutureCont(lInf, lSup)));
            }
            lInf = lSup;
            lSup += tVentana;
        }

        int contAux = 0;

        for (Future<Integer> f: future) {
            try {
                contAux += f.get();
            } catch (CancellationException e) {}
            catch (ExecutionException e) {}
            catch (InterruptedException e) {}
        }

        pool.shutdown();
        try {
            pool.awaitTermination(5L, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {}

 
        System.out.println("Ha tardado: " + (((double)System.currentTimeMillis() - iniCronom) / 1000) + " segundos");
        System.out.println("Aproximacion: " + ((double) contAux / nPuntos));
    }
}
