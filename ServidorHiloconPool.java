/**
 * Práctica 6 - PCTR
 * 2017/18
 *
 * @author Iván Magariño Aguilar
 */

import java.net.*;
import java.util.concurrent.*;
import java.io.*;

public class ServidorHiloconPool implements Runnable {

    //Socket donde realizar la conexion
    Socket enchufe;

    /**
     * Constructor de la clase
     * @param s
     */
    public ServidorHiloconPool(Socket s) {
        enchufe = s;
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run() {
        try {
            BufferedReader entrada = new BufferedReader(new InputStreamReader(enchufe.getInputStream()));
            String datos = entrada.readLine();
            int j;
            int i = Integer.valueOf(datos).intValue();
            for (j = 1; j <= 20; j++) {
                System.out.println("El hilo " + Thread.currentThread().getName() + " escribiendo el dato " + i);
                Thread.sleep(1000);
            }
            enchufe.close();
            System.out.println("El hilo " + Thread.currentThread().getName() + "cierra su conexion...");
        } catch (Exception e) {
            System.out.println("Error...");
        }
    }// run

    /**
     * Metodo principal
     * @param args
     */
    public static void main(String[] args) {
        int puerto = 2001;

        ExecutorService executor = Executors.newCachedThreadPool();

        try {
            ServerSocket chuff = new ServerSocket(puerto, 3000);
            while (true) {
                System.out.println("Esperando solicitud de conexion...");
                Socket cable = chuff.accept();
                System.out.println("Recibida solicitud de conexion...");
                executor.execute(new ServidorHiloconPool(cable));
            } // while
        } catch (Exception e) {
            System.out.println("Error en sockets...");
        }
        finally{
            executor.shutdown();
            try{executor.awaitTermination(1L, TimeUnit.MINUTES);}
            catch(InterruptedException ex){}
        }
    }// main
}// Servidor_Hilos
