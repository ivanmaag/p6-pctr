/**
 * Práctica 6 - PCTR
 * 2017/18
 *
 * @author Iván Magariño Aguilar
 */

import java.util.Random;
import java.util.Scanner;

public class IntParalelaunitCont implements Runnable {

    //Contador de exitos
    public static int cont;
    
    //Cerrojo compartido por las instancias
    public static Object lock;
    
    //Rango a evaluar por cada instancia
    public int lInf, lSup;
    
    //Variable Random para generar los puntos 
    public Random r;

    /**
     * Constructor vacio de la clase
     */
    public IntParalelaunitCont(){}

    /**
     * Constructor de la clase
     * @param lInf limite inferior del subrango
     * @param lSup limite superior del subrango
     */
    public IntParalelaunitCont(int lInf, int lSup) {
        this.lInf = lInf;
        this.lSup = lSup;
        cont = 0;
        r = new Random(System.currentTimeMillis());
        lock = new Object();
    }

    public int GetCont() {
        return cont;
    }

    public void run() {
        for (int i = lInf; i < lSup; i++) {
            if (r.nextDouble() < Math.sin(r.nextDouble())) {
                synchronized(lock) {
                    cont++;
                }
            }
        }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main (String[] args) {
        
        Scanner s = new Scanner(System.in);
        int nProcesos = Runtime.getRuntime().availableProcessors();
        System.out.print("Introduzca numero de puntos: ");
        int nPuntos = s.nextInt();
        int tVentana = nPuntos / nProcesos;
        int lInf = 0;
        int lSup = tVentana;
        Thread[] hilos = new Thread[nProcesos];

        long iniCronom = System.currentTimeMillis();
        for (int i = 0; i < nProcesos - 1; i++) {
            hilos[i] = new Thread(new IntParalelaunitCont(lInf, lSup));
            hilos[i].start();
            lInf = lSup;
            lSup += tVentana;
        }

        hilos[nProcesos - 1] = new Thread(new IntParalelaunitCont(lInf, nPuntos));
        hilos[nProcesos - 1].start();

        for (int i = 0; i < nProcesos; i++) {
            try {
                hilos[i].join();
            } catch (InterruptedException ex) {}
        }
        
        System.out.println("Ha tardado: " + (((double)System.currentTimeMillis() - iniCronom) / 1000) + " segundos");
        IntParalelaunitCont observer = new IntParalelaunitCont();
        System.out.println("Aproximacion: " + ((double)observer.GetCont() / nPuntos));
    }
}
