/**
 * Práctica 6 - PCTR
 * 2017/18
 *
 * @author Iván Magariño Aguilar
 */

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class IntParalelomultiCont implements Runnable {

    private int cont;

    private int lInf, lSup;

    private Random r;

    /**
     * Constructor de la clase
     * @param lInf limite inferior del subrango
     * @param lSup limite superior del subrango
     */
    public IntParalelomultiCont(int lInf, int lSup) {
        this.lInf = lInf;
        this.lSup = lSup;
        cont = 0;
        r = new Random(System.currentTimeMillis());
    }

    public int GetCont() {
        return cont;
    }

    /**
     * Sobrecarga del metodo run
     */
    public void run() {
        for (int i = lInf; i < lSup; i++) {
            if (r.nextDouble() < Math.sin(r.nextDouble())) {
                cont++;
            }
        }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int nProcesos = Runtime.getRuntime().availableProcessors();
        System.out.print("Introduzca numero de puntos: ");
        int nPuntos = s.nextInt();
        int tVentana = nPuntos / nProcesos;
        int lInf = 0;
        int lSup = tVentana;
        IntParalelomultiCont[] intP = new IntParalelomultiCont[nProcesos];
        
        ThreadPoolExecutor pool = new ThreadPoolExecutor(nProcesos, nProcesos, 1L, TimeUnit.MINUTES,
                new LinkedBlockingQueue<Runnable>());
        
        long iniCronom = System.currentTimeMillis();
        for (int i = 0; i < nProcesos - 1; i++) {
            intP[i] = new IntParalelomultiCont(lInf, lSup); 
            pool.submit(intP[i]);
            lInf = lSup;
            lSup += tVentana;
        }

        intP[nProcesos - 1] = new IntParalelomultiCont(lInf, nPuntos);
        pool.submit(intP[nProcesos - 1]);
        pool.shutdown();
        try {
            pool.awaitTermination(1L, TimeUnit.MINUTES);
        } catch (InterruptedException ex) {}
   
        System.out.println("Ha tardado: " + (((double)System.currentTimeMillis() - iniCronom) / 1000) + " segundos");

        int contAux = 0;
        for (int i = 0; i < nProcesos; i++) {
            contAux += intP[i].GetCont();
        }

        System.out.println("Aproximacion: " + ((double) contAux / nPuntos));
    }
}
