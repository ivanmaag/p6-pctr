/**
 * Práctica 6 - PCTR
 * 2017/18
 *
 * @author Iván Magariño Aguilar
 */

import java.net.*;
import java.io.*;

public class ClienteMultiple {

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {
        int puerto = 2001;
        try {
            long iniCronom = System.currentTimeMillis();
            Socket[] cables = new Socket[3000];
            for (int i = 0; i < cables.length; i++) {
                cables[i] = new Socket("localhost", puerto);
                PrintWriter salida = new PrintWriter(
                        new BufferedWriter(new OutputStreamWriter(cables[i].getOutputStream())));
                salida.println((int) (Math.random() * 10));
                salida.flush();
                cables[i].close();
            }

            System.out.println("Ha tardado -> " + (((double) System.currentTimeMillis() - iniCronom) / 1000) + " segundos");

        } // try
        catch (Exception e) {
            System.out.println("Error en sockets...");
        }
    }// main
}// Cliente_Hilos
