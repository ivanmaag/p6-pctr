/**
 * Práctica 6 - PCTR
 * 2017/18
 *
 * @author Iván Magariño Aguilar
 */

public class CuentaCorrienteSegura {

    //Variable compartida
    private double saldo;

    public CuentaCorrienteSegura(double saldo) {
        this.saldo = saldo;
    }

    public synchronized double GetSaldo() {
        return saldo;
    }

    public synchronized void Ingreso() {
        saldo++;
    }

    public synchronized void Reintegro() {
        saldo--;
    }
}
