/**
 * Práctica 6 - PCTR
 * 2017/18
 *
 * @author Iván Magariño Aguilar
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class FicheroSeguro extends Thread {
    
    //Referencia compartida por todos los objetos para usarse de cerrojo 
    private static RandomAccessFile fichero;

    public FicheroSeguro() {
        try {
            fichero = new RandomAccessFile("prueba.dat", "rw");
        } catch (FileNotFoundException fx) {}
    }

    public void run() {
        synchronized(fichero) {
            try {
                fichero.writeChars(""+Math.random()*10+'\n');
            } catch (IOException io) {}
       }
    }

    /**
     * Metodo main
     * @param args
     */
    public static void main(String[] args) {

        FicheroSeguro[] threads = new FicheroSeguro[10];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new FicheroSeguro();
            threads[i].start();
        }

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException ex) {}
        }
    }
}
